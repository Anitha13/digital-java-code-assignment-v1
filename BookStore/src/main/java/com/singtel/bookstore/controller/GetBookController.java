package com.singtel.bookstore.controller;

/** GetBookController
 * 
 * 
 */

import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.singtel.bookstore.beans.Book;
import com.singtel.bookstore.beans.BookRegisterRequest;

/** Controller to get all the Book records **/

@Controller
public class GetBookController {
	
	@RequestMapping(method = RequestMethod.GET, value="/book/allbooks")
	@ResponseBody
	
	  public List<Book> getAllBooks() {
		return BookRegisterRequest.getInstance().getBooks();
	  }

}

package com.singtel.bookstore.controller;

/** InsertBookController
 * 
 */

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.singtel.bookstore.beans.Book;
import com.singtel.bookstore.beans.BookRegisterRequest;
import com.singtel.bookstore.beans.BookRegisterResponse;


/** Controller class to insert Book records **/

@Controller
public class InsertBookController {

	@RequestMapping(method = RequestMethod.POST, value="/register/book")
	
	@ResponseBody	
	public BookRegisterResponse registerBook(@RequestBody Book book) {
	  System.out.println("In register Book");
	  BookRegisterResponse stdresponse = new BookRegisterResponse();           
	  BookRegisterRequest.getInstance().add(book);
	        //We are setting the below value just to reply a message back to the caller
	  stdresponse.setGenre(book.getGenre());
	  stdresponse.setISBN(book.getISBN());
	  stdresponse.setResponse("Successful");
	  return stdresponse;
	}
}

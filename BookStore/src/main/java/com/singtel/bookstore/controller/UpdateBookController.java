package com.singtel.bookstore.controller;

/** UpdateBookController
 * 
 *   
 */


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.singtel.bookstore.beans.Book;
import com.singtel.bookstore.beans.BookRegisterRequest;
import com.singtel.bookstore.beans.BookRegisterResponse;


/** Controller class to update the Book record **/

@Controller
public class UpdateBookController {
	
	@RequestMapping(method = RequestMethod.PUT, value="/update/book")
	@ResponseBody
	
	public BookRegisterResponse updateBooks(@RequestBody Book book) {
	System.out.println("In update Book");   
	
		BookRegisterResponse stdresponse = new BookRegisterResponse();           
		 String updateResponse = BookRegisterRequest.getInstance().upDateBookStore(book);
		        //We are setting the below value just to reply a message back to the caller
		  stdresponse.setGenre(book.getGenre());
		  stdresponse.setISBN(book.getISBN());
		  stdresponse.setResponse(updateResponse);
		  return stdresponse;
	}
}

package com.singtel.bookstore.beans;

/** BookRegisterResponse
 * 
 * @author ahanumantha4
 * 
 * POJO class to return the response to the caller
 *
 */


public class BookRegisterResponse {
	
	String ISBN;
	String genre;
	String response;
	
	
	public String getISBN() {
		return ISBN;
	}
	public void setISBN(String ISBN) {
		this.ISBN = ISBN;
	}
	public String getGenre() {
		return genre;
	}
	public void setGenre(String genre) {
		this.genre = genre;
	}
	public String getResponse() {
		return response;
	}
	public void setResponse(String response) {
		this.response = response;
	}
	

}

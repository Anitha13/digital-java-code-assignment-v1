package com.singtel.bookstore.beans;

import java.util.List;

public class Stationary {

	List<Pen> pen;
	List<Pencil> pencil;
	List<Book> books;
	
	public List<Pen> getPen() {
		return pen;
	}
	public void setPen(List<Pen> pen) {
		this.pen = pen;
	}
	public List<Pencil> getPencil() {
		return pencil;
	}
	public void setPencil(List<Pencil> pencil) {
		this.pencil = pencil;
	}
	public List<Book> getBooks() {
		return books;
	}
	public void setBooks(List<Book> books) {
		this.books = books;
	}
}

package com.singtel.bookstore.beans;

/** Pen POJO class
 * 
 * @author ahanumantha4
 *  refill - indicates refill type
 *  quantity - number of pen
 *  penType - indicate type of the pen. Ex - Ballpoint pen, Ink pen etc--
 *
 */

public class Pen {

	String refill;
	int quantity;
	String penType;
	
	public String getRefill() {
		return refill;
	}
	public void setRefill(String refill) {
		this.refill = refill;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public String getPenType() {
		return penType;
	}
	public void setPenType(String penType) {
		this.penType = penType;
	}
}

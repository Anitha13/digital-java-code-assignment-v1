package com.singtel.bookstore.beans;

/** Pencil POJO class
 * 
 * @author ahanumantha4
 * 
 * pencilType - indicate type of the pencil . Ex- HB pencil, others
 * pencilQuantity - number of pencils
 *
 */


public class Pencil {
	
	String pencilType;
	int pencilQuantity;
	
	public String getPencilType() {
		return pencilType;
	}
	public void setPencilType(String pencilType) {
		this.pencilType = pencilType;
	}
	public int getPencilQuantity() {
		return pencilQuantity;
	}
	public void setPencilQuantity(int pencilQuantity) {
		this.pencilQuantity = pencilQuantity;
	}
	

}

package com.singtel.bookstore.beans;

/** BookRegisterRequest 
 * 
 * Handles the request from the API call
 * 
 */


import java.util.ArrayList;
import java.util.List;

public class BookRegisterRequest {

	private List<Book> book;
    private static BookRegisterRequest bookRegister = null;
    
    private BookRegisterRequest(){
    	book = new ArrayList<Book>();
    }
    
    public static BookRegisterRequest getInstance() {
        if(bookRegister == null) {
        	bookRegister = new BookRegisterRequest();
              return bookRegister;
            }
            else {
                return bookRegister;
            }
    }
    
    
    public void add(Book std) {
    	book.add(std);
    }
    
    public String upDateBookStore(Book std) {
    	for(int i=0; i<book.size(); i++)
    	        {
    	            Book stdn = book.get(i);
    	            if(stdn.getISBN().equals(std.getISBN())) {
    	              book.set(i, std);//update the new record
    	              return "Update successful";
    	            }
    	        }
    	return "Update un-successful";
    	}
    	    
    
    public List<Book> getBooks() {
    	    return book;
   }
}

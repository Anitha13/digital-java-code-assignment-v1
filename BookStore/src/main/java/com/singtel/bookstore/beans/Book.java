package com.singtel.bookstore.beans;

/** Book POJO class 
 * 
 * @author ahanumantha4
 * 
 * ISBN - unique identifier
 * Genre - type of book
 */

public class Book {
	
	String ISBN;
	String genre;
	
	
	public String getISBN() {
		return ISBN;
	}
	public void setISBN(String ISBN) {
		this.ISBN = ISBN;
	}
	public String getGenre() {
		return genre;
	}
	public void setGenre(String genre) {
		this.genre = genre;
	}
	
}
